# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Interface traffic shaping."""

import argparse
import os
import subprocess
from configparser import ConfigParser

__version__ = '0.0.2'

# Set default configuration file
CONFIG_FILE = '/etc/opentrafficshaper/interfaces.conf'


def run_tc(*args: str, ignore_ret=False):
    """Run tc."""

    tc_args = ['tc']
    tc_args.extend(args)

    try:
        subprocess.check_output(tc_args, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as exception:
        # Check if we ignoring the result
        if not ignore_ret:
            print('ERROR: %s' % exception.output.decode('utf-8'))
            exit(1)


def set_rate_out(interface: str, rate_out: int):
    """Set outbound rate limit."""

    speed_low = float(rate_out) * 0.95
    speed_high = float(rate_out) * 0.98

    # Set packet limits to a sane value
    limit_packets = int(float(rate_out) * 50)
    if limit_packets < 100:
        limit_packets = 100
    elif limit_packets > 10000:
        limit_packets = 10000

    # Set flows based on the speed
    flows = int(float(limit_packets) / 10)

    # Set flow depth
    depth = flows * 2

    # Set hash size
    hash_size = 2**(limit_packets - 1).bit_length()
    if hash_size < 1024:
        hash_size = 1024

    # Flow depth * half packet size * 30% queue size
    redflowlimit = int(depth * 750)

    print(f'Rate out........: {rate_out}Mbit')
    print(f'Speed low.......: {speed_low}Mbit')
    print(f'Speed high......: {speed_high}Mbit')
    print(f'Limit packets...: {limit_packets}')
    print(f'Hash size.......: {hash_size}')
    print(f'Flows...........: {flows}')
    print(f'Flow depth......: {depth}')
    print(f'Flow threshold..: {redflowlimit}')

    # Clear root qdisc
    run_tc('qdisc', 'del', 'dev', interface, 'root', ignore_ret=True)

    # Add root qdisc
    run_tc('qdisc', 'add', 'dev', interface, 'root', 'handle', '1:', 'hfsc', 'default', '1')

    # Add class
    run_tc('class', 'add', 'dev', interface, 'parent', '1:', 'classid', '1:1',
           'hfsc', 'sc', 'rate', f'{speed_low}mbit', 'ul', 'rate', f'{speed_high}mbit')

    # Add SFQ qdisc
    run_tc('qdisc', 'add', 'dev', interface, 'parent', '1:1', 'handle', '10:',
           'sfq', 'perturb', '10',
           'divisor', f'{hash_size}',
           'limit', f'{limit_packets}',
           'depth', f'{depth}',
           'flows', f'{flows}',
           'redflowlimit', f'{redflowlimit}',
           'ecn')


def set_rate_in(interface: str, rate_in: int):
    """Set inbound rate limit."""

    run_tc('qdisc', 'del', 'dev', interface, 'ingress', ignore_ret=True)

    burst = int((float(rate_in) * 1024) / 8 / 2)

    print(f'Rate in.........: {rate_in}Mbit')
    print(f'Burst size......: {burst}k')

    # Add qdisc ingress
    run_tc('qdisc', 'add', 'dev', interface, 'ingress')

    # Add filter for inbound policing
    run_tc('filter', 'add', 'dev', interface, 'parent', 'ffff:',
           'u32', 'match', 'u32', '0', '0',
           'police', 'rate', f'{rate_in}mbit', 'burst', f'{burst}k', 'mtu', '64kb', 'drop')


def main():
    """Entry point for execution from the commandline."""

    print(f'AllWorldIT Interface Shaper v{__version__} - Copyright © 2019, AllWorldIT.\n')

    # Start argument parser
    argparser = argparse.ArgumentParser(add_help=False)
    # Create argument group for rate limit options
    rate_group = argparser.add_argument_group('Rate limit options')
    rate_group.add_argument('interface', metavar='IFACE', nargs=1,
                            help='Interface name')
    rate_group.add_argument('rate_out', metavar='RATE-OUT', nargs='?',
                            help='Manually specify outbound traffic rate limit')
    rate_group.add_argument('rate_in', metavar='RATE-IN', nargs='?',
                            help='Manually specify inbound traffic rate limit')
    # Create argument group for optionals
    optional_group = argparser.add_argument_group('Optional arguments')
    optional_group.add_argument('-h', '--help', action="help", help="Show this help message and exit")
    optional_group.add_argument('-c', '--config-file', dest='config_file', action='store',
                                help=f'Use specified configuration file instead of "{CONFIG_FILE}"')

    # Parse args
    args = argparser.parse_args()

    # Grab interfac
    interface = args.interface[0]

    # Default rates to None
    rate_out = None
    if args.rate_out:
        rate_out = args.rate_out[0]

    rate_in = None
    if args.rate_in:
        rate_in = args.rate_in[0]

    # Check if we need to override the config file
    config_file = CONFIG_FILE
    if args.config_file:
        config_file = args.config_file

    # If rate out was not specified, see if we can pull from the config file
    if (not rate_out) and os.path.exists(config_file):
        # Read config file
        with open(config_file, 'r') as cfile:
            config_data = cfile.read()

        # Read config as string
        config_parser = ConfigParser()
        config_parser.read_string(config_data)

        # Check if we have an interface section
        if not config_parser.has_section(interface):
            print(f'ERROR: Interface "{interface}" not found in config file "{config_file}"')
            exit(1)

        # If we don't have a rate-out option for this interface, its a problem
        if not config_parser.has_option(interface, 'rate-out'):
            print(f'ERROR: Interface "{interface}" has no "rate-out" option in config file "{config_file}"')
            exit(1)

        # If we do grab it
        rate_out = int(config_parser.get(interface, 'rate-out'))

        # Check if we have an inbound rate
        if config_parser.has_option(interface, 'rate-in'):
            rate_in = int(config_parser.get(interface, 'rate-in'))

    # If we still don't have a rate_out, throw an error
    if not rate_out:
        print(f'ERROR: No rate limits specified for "{interface}" on commandline or in config file "{config_file}"')
        exit(1)

    # Set rate out and rate in
    print(f'Interface.......: {interface}\n')
    set_rate_out(args.interface[0], rate_out)
    if rate_in:
        print('')
        set_rate_in(args.interface[0], rate_in)
